package com.opsig.sig;

import com.opsig.sig.database.*;
import com.opsig.sig.gui.Label;
import com.opsig.sig.gui.LineString;
import com.opsig.sig.gui.MapPanel;
import com.opsig.sig.gui.Point;
import com.opsig.sig.gui.Polygon;
import java.awt.Color;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.WindowConstants;
import org.postgis.Geometry;
import org.postgis.PGgeometry;

/**
 * Main class used to run the tests
 */
public class SigRunner {

    public static void main(String[] args) throws SQLException {
        int questionNumber = 0;

        try {
            questionNumber = Integer.parseInt(args[0]);
        } catch (NumberFormatException e) {

        }

        switch (questionNumber) {
            case 8:
                q8_simpleConnection();
                break;
            case 9:
                q9_searchLikeRequest();
                break;
            case 10:
                q10b_displayMapWithBuildings();
                break;
            case 11:
                q11c_buildingConcentration();
                break;
            default:
                System.out.println("Please pass question number as parameter");
        }
    }

    /**
     * Connect to database test
     */
    private static void q8_simpleConnection() {
        Utils.getConnection();
        Utils.closeConnection();
    }

    /**
     * Search with a like test
     *
     * @throws SQLException
     */
    private static void q9_searchLikeRequest() throws SQLException {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter your search query like parameter:");
        String likeString = scanner.nextLine();

        System.out.println("Searching the database, this might take a few seconds...");

        //create request from parameter
        PreparedStatement statement
                = Utils.getConnection().prepareStatement("SELECT tags->'name', geom "
                        + "FROM nodes "
                        + "WHERE tags->'name' LIKE ?");
        statement.setString(1, likeString);

        //get results
        ResultSet result = statement.executeQuery();

        //display results
        while (result.next()) {
            PGgeometry nodeGeometry = (PGgeometry) (result.getObject(2));
            System.out.println(result.getString(1)
                    + " | "
                    + nodeGeometry.getGeometry().getFirstPoint().x
                    + " | "
                    + nodeGeometry.getGeometry().getFirstPoint().y);
        }

        Utils.closeConnection();
    }

    /**
     * Display all buildings around Grenoble
     *
     * @throws SQLException
     */
    private static void q10b_displayMapWithBuildings() throws SQLException {

        //create new Frame
        JFrame appFrame = new JFrame();
        appFrame.setSize(1024, 768);

        //Configures application closing and confirmation
        addCloseAction(appFrame);

        //Initialize map
        MapPanel panel = new MapPanel(5.75, 45.15, 0.1);
        panel.setVisible(true);

        System.out.println("Searching Grenoble's buildings, they shall be "
                + "displayed once the search is complete.");

        //create request
        PreparedStatement statement
                = Utils.getConnection().prepareStatement("SELECT linestring FROM ways "
                        + "WHERE tags->'building' = 'yes' AND "
                        + "ST_X(ST_Centroid(linestring)) BETWEEN 5.7 AND 5.8 AND "
                        + "ST_y(ST_Centroid(linestring)) BETWEEN 45.1 AND 45.2");
        ResultSet result = statement.executeQuery();

        //Draw result
        while (result.next()) {
            //get result object
            PGgeometry buildingPGGeometry = (PGgeometry) (result.getObject(1));

            //get result object geometry
            Geometry buildingGeometry = buildingPGGeometry.getGeometry();

            LineString currentLine = new LineString();

            //add each element to the line
            for (int i = 0; i < buildingGeometry.numPoints(); i++) {
                Point currentPoint = new Point(buildingGeometry.getPoint(i).x,
                        buildingGeometry.getPoint(i).y);
                currentLine.addPoint(currentPoint);
            }

            //display line on panel
            panel.addPrimitive(currentLine);
        }

        appFrame.add(panel);
        appFrame.setVisible(true);
        Utils.closeConnection();
    }

    /**
     * Display building concentration on a map
     *
     * @throws SQLException
     */
    private static void q11c_buildingConcentration() throws SQLException {

        /**
         * defines how many divisions the concentration calculation will take
         * into consideration have in mind that the total number of subdivisions
         * on the map equals to concDivX time concDivY
         */
        final int concDivX = 50, concDivY = 50;

        //maps limits
        double minY = 45.1, maxY = 45.2, minX = 5.7, maxX = 5.8;

        //Create new Frame
        JFrame appFrame = new JFrame();
        appFrame.setSize(1024, 768);

        //Configures application closing and confirmation
        addCloseAction(appFrame);

        //starts new map parameters to include all cities
        MapPanel panel = new MapPanel(
                ((minX + maxX) / 2),
                ((minY + maxY) / 2),
                Math.max(maxX - minX, maxY - minY));

        panel.setVisible(true);

        System.out.println("Calcultating the building concentration on "
                + "Rhône-Alpes, it shall be displayed once the calculation is done.");

        System.out.println("Consulting cities of the Rhône-alpes region...");

        //create request
        PreparedStatement labelStatement
                = Utils.getConnection()
                .prepareStatement("SELECT geom, tags->'name' "
                        + "FROM nodes "
                        + "WHERE tags->'place' = 'city'");

        //execute request
        ResultSet resultLabel = labelStatement.executeQuery();

        while (resultLabel.next()) {
            //get result object
            PGgeometry cityPGGeometry = (PGgeometry) (resultLabel.getObject(1));

            //get result object geometry
            Geometry cityGeometry = cityPGGeometry.getGeometry();

            //get city center for label
            Point cityPoint = new Point(cityGeometry.getFirstPoint().x,
                    cityGeometry.getFirstPoint().y);

            //create named label
            Label cityLabel = new Label(Color.black, resultLabel.getString(2), cityPoint);

            //add label to panel (map)
            panel.addPrimitive(cityLabel);
        }

        System.out.println("Calculating building concentration on the Rhône-Alpes region..");

        String buildingRequestStr = "SELECT ST_X(w.cent), ST_Y(w.cent) FROM ("
                + "SELECT ST_Centroid(linestring) as cent "
                + "FROM ways WHERE tags->'building' = 'yes') as w "
                + "WHERE ST_X(w.cent) BETWEEN ? AND ? "
                + "AND ST_Y(w.cent) BETWEEN ? AND ?";

        PreparedStatement buildingRequestStatement
                = Utils.getConnection().prepareStatement(buildingRequestStr);

        buildingRequestStatement.setDouble(1, minX);
        buildingRequestStatement.setDouble(2, maxX);
        buildingRequestStatement.setDouble(3, minY);
        buildingRequestStatement.setDouble(4, maxY);

        ResultSet buildingResult = buildingRequestStatement.executeQuery();

        int[][] concentrationMatrix = new int[concDivX][concDivY]; //initial value 0 for all elementes guaranteed on the language spec
        double buildingX, buildingY;

        while (buildingResult.next()) {
            buildingX = buildingResult.getDouble(1);
            buildingY = buildingResult.getDouble(2);

            /* IGNORES UNWANTED BUILDINGS
             this consideration could is done inside the request, but it
             could also be done here.
             */
//            if (buildingX < minX || buildingX >= maxX) {
//                continue;
//            }
//            if (buildingY < minY || buildingY >= maxY) {
//                continue;
//            }
            int concIndexX = (int) (Math.floor((buildingX - minX) / ((maxX - minX) / concDivX)));
            int concIndexY = (int) (Math.floor((buildingY - minY) / ((maxY - minY) / concDivY)));
            concentrationMatrix[concIndexX][concIndexY]++;

        }

        int maxConcentration = 0;
        for (int i = 0; i < concDivX; i++) {
            for (int j = 0; j < concDivY; j++) {
                //calculates the maximum concentration in order to normalize the concentration color
                //(maximum implies in alpha = 255
                if (concentrationMatrix[i][j] > maxConcentration) {
                    maxConcentration = concentrationMatrix[i][j];
                }
            }
        }

        //draws the concentration rectangles
        for (int i = 0; i < concDivX; i++) {
            for (int j = 0; j < concDivY; j++) {
                int alpha = (concentrationMatrix[i][j] * 255) / maxConcentration;
                Polygon concPolygon = new Polygon(new Color(255, 0, 0, alpha), new Color(255, 0, 0, alpha));

                concPolygon.addPoint(new Point(minX + i * ((maxX - minX) / concDivX), //x
                        minY + j * ((maxY - minY) / concDivY))); //y
                concPolygon.addPoint(new Point(minX + (i + 1) * ((maxX - minX) / concDivX), //x
                        minY + j * ((maxY - minY) / concDivY))); //y
                concPolygon.addPoint(new Point(minX + (i + 1) * ((maxX - minX) / concDivX), //x
                        minY + (j + 1) * ((maxY - minY) / concDivY))); //y
                concPolygon.addPoint(new Point(minX + i * ((maxX - minX) / concDivX), //x
                        minY + (j + 1) * ((maxY - minY) / concDivY))); //y

                panel.addPrimitive(concPolygon);
            }
        }

        appFrame.add(panel);
        appFrame.setVisible(true);
        Utils.closeConnection();
    }

    /**
     * Used to close the program while closing the frame
     *
     * @param appFrame the frame to be closed
     */
    private static void addCloseAction(JFrame appFrame) {
        appFrame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        appFrame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                JFrame frame = (JFrame) e.getSource();

                int result = JOptionPane.showConfirmDialog(
                        frame,
                        "Are you sure you want to exit the application?",
                        "Exit Application",
                        JOptionPane.YES_NO_OPTION);

                if (result == JOptionPane.YES_OPTION) {
                    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                }
            }
        });
    }
}
