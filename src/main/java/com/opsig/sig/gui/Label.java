package com.opsig.sig.gui;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics2D;

/**
 * Represents label. Basically a string.
 *
 * @author Gabriel S.
 * @version 1.0
 */
public class Label implements GraphicalPrimitive {

    /**
     * Label's color
     */
    private Color drawColor = new Color(50, 50, 50, 255);

    /**
     * Label's text
     */
    private String text;

    /**
     * Label's position
     */
    private Point position;

    /**
     * Label's bounding box
     */
    private BoundingBox boundingBox;

    /**
     * Builds and initializes a string in a certain position.
     *
     * @param dc the drawing color.
     * @param txt the label's text
     * @param pos the label's position
     */
    public Label(Color dc, String txt, Point pos) {
        this.drawColor = dc;
        this.text = txt;
        this.position = pos;
    }

    @Override
    public void draw(Graphics2D g2d, CoordinateConverter converter) {
        Color oldDC = g2d.getColor();
        g2d.setColor(this.drawColor);

        double x = position.getX();
        double y = position.getY();

        g2d.drawString(text, converter.xMapToScreen(x), converter.yMapToScreen(y));

        g2d.setColor(oldDC);

        //computes boundingbox here because it takes the metrics of g2d to compute it
        FontMetrics metrics = g2d.getFontMetrics();
        int height = metrics.getHeight();
        int width = metrics.stringWidth(text);

        boundingBox = new BoundingBox(x, y, x + converter.xScreenToMap(width), y + converter.yScreenToMap(width));

    }

    @Override
    public BoundingBox getBoundingBox() {
        return boundingBox;
    }
}
